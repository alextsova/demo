package com.sample.test.demo.tests;

import com.sample.test.demo.constants.Locators;
import com.sample.test.demo.constants.PizzaToppings;
import com.sample.test.demo.constants.PizzaTypes;
import com.sample.test.demo.tests.base.BaseTest;
import com.sample.test.demo.utils.StringUtils;
import com.sample.test.demo.utils.testdata.Pizza;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Epic("Verification Of Field Value in the order pizza page")
public class VerificationOfFieldValueTest extends BaseTest {

    private int lengthOfEnableStringValueForField = 100;

    @Test(description = "Enter text as quantity of pizza")
    @Description("Verify that when we enter text as number of pizza we see an error")
    @Story("Verify quantity field")
    public void verifyQuantityFieldTest() {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.SMALL_ONETOPPINGS.getDisplayName())
                .firstTopping(PizzaToppings.OLIVES.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza("qwe");
        orderPizzaPage.enterDefaultContactInformation();
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with error information about filled up the quantity fields")
                .contains("Wrong quantity value");

        orderPizzaPage.closeDialogMessage();
    }

    @Test(description = "Enter empty as quantity of pizza")
    @Description("Verify that when we add empty value as number of pizza we see an error")
    @Story("Verify quantity field")
    public void verifyQuantityFieldEmptyValueTest() {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.SMALL_ONETOPPINGS.getDisplayName())
                .firstTopping(PizzaToppings.OLIVES.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza("");
        orderPizzaPage.enterDefaultContactInformation();
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with error information about filled up the quantity fields")
                .contains("Wrong quantity value");

        orderPizzaPage.closeDialogMessage();
    }

    @Test(description = "Verify max length of name filed")
    @Description("Verify that when we can add only 100 chars in the name field")
    @Story("Verify length of fields")
    public void verifyMaxLengthOfNameFiledTest() {
        orderPizzaPage.filledUpNameField(StringUtils.randomStringWith101Chars());

        assertThat(orderPizzaPage.getValueOfField(Locators.NAME_FIELD.getByLocators()).length())
                .as("Verify that length of field is 100")
                .isEqualTo(lengthOfEnableStringValueForField);
    }

    @Test(description = "Verify max length of email filed")
    @Description("Verify that when we can add only 100 chars in the email field")
    @Story("Verify length of fields")
    public void verifyMaxLengthOfEmailFiledTest() {
        orderPizzaPage.filledUpEmailField(StringUtils.randomStringWith101Chars());

        assertThat(orderPizzaPage.getValueOfField(Locators.EMAIL_FILED.getByLocators()).length())
                .as("Verify that length of field is 100")
                .isEqualTo(lengthOfEnableStringValueForField);
    }

    @Test(description = "Verify max length of phone filed")
    @Description("Verify that when we can add only 100 chars in the phone field")
    @Story("Verify length of fields")
    public void verifyMaxLengthOfPhoneFiledTest() {
        orderPizzaPage.filledUpPhoneField(StringUtils.randomStringWith101Chars());

        assertThat(orderPizzaPage.getValueOfField(Locators.PHONE_FIELD.getByLocators()).length())
                .as("Verify that length of field is 100")
                .isEqualTo(lengthOfEnableStringValueForField);
    }
}
