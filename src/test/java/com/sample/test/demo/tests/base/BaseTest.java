package com.sample.test.demo.tests.base;

import com.google.common.collect.ImmutableMap;
import com.sample.test.demo.pages.OrderPizzaPage;
import com.sample.test.demo.utils.PropertyConfiguration;
import com.sample.test.demo.utils.allure.TestListener;
import com.sample.test.demo.utils.driver.DriverProvider;
import com.sample.test.demo.utils.driver.IWebDriver;
import io.qameta.allure.Step;
import org.testng.annotations.*;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;

@Listeners(TestListener.class)
public class BaseTest {

    private PropertyConfiguration config;
    protected IWebDriver webDriverImplementation;
    protected OrderPizzaPage orderPizzaPage;
    protected String pizzaQuantity = "1";

    @Step("Global configuration")
    @BeforeClass(alwaysRun = true)
    public void setUp() {
        config = PropertyConfiguration.getInstance();
        setEnvValuesToAllure();
        webDriverImplementation = new DriverProvider().getDriver();

        orderPizzaPage = new OrderPizzaPage(webDriverImplementation);
        orderPizzaPage.openPage(config.getUrl());
    }

    @Step("Close browser")
    @AfterClass(alwaysRun = true)
    public void globalTearDown() {
        webDriverImplementation.quit();
    }

    public IWebDriver getDriver() {
        return webDriverImplementation;
    }

    private void setEnvValuesToAllure() {
        allureEnvironmentWriter(
                ImmutableMap.<String, String>builder()
                        .put("Environment", config.getPlatform())
                        .put("Browser", config.getBrowser())
                        .build());
    }
}
