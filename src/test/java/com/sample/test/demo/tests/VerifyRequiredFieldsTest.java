package com.sample.test.demo.tests;

import com.sample.test.demo.constants.Locators;
import com.sample.test.demo.constants.PizzaToppings;
import com.sample.test.demo.constants.PizzaTypes;
import com.sample.test.demo.tests.base.BaseTest;
import com.sample.test.demo.utils.testdata.Pizza;
import com.sample.test.demo.utils.testdata.UserProvider;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Epic("Verify Required fields on the order page")
public class VerifyRequiredFieldsTest extends BaseTest {

    @Test(description = "No enter any required data into fields")
    @Description("Verify that when we do not enter any data for required fields we see error where asked add them")
    @Story("Verify Required fields")
    public void verifyAllRequiredFiledTest() {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.SMALL_ONETOPPINGS.getDisplayName())
                .firstTopping(PizzaToppings.OLIVES.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza(pizzaQuantity);
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with error information about filled up the required fields")
                .contains("Missing name")
                .contains("Missing phone number");

        orderPizzaPage.closeDialogMessage();
    }

    @Test(description = "No enter any required phone number into field")
    @Description("Verify that when we do not enter phone number for required fields we see error where asked add it")
    @Story("Verify Required fields")
    public void verifyPhoneRequiredFiledTest() {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.SMALL_ONETOPPINGS.getDisplayName())
                .firstTopping(PizzaToppings.OLIVES.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza(pizzaQuantity);
        orderPizzaPage.filledUpNameField(UserProvider.getDefaultUser().getUserName());
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with error information about filled up the required field")
                .contains("Missing phone number");

        orderPizzaPage.closeDialogMessage();
    }

    @Test(description = "No enter any required name into field")
    @Description("Verify that when we do not enter name for required fields we see error where asked add it")
    @Story("Verify Required fields")
    public void verifyNameRequiredFiledTest() {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.SMALL_ONETOPPINGS.getDisplayName())
                .firstTopping(PizzaToppings.OLIVES.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza(pizzaQuantity);
        orderPizzaPage.filledUpPhoneField(UserProvider.getDefaultUser().getPhoneNumber());
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with error information about filled up the required field")
                .contains("Missing name");

        orderPizzaPage.closeDialogMessage();
    }
}
