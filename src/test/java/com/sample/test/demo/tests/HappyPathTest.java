package com.sample.test.demo.tests;

import com.sample.test.demo.constants.Locators;
import com.sample.test.demo.constants.PizzaToppings;
import com.sample.test.demo.constants.PizzaTypes;
import com.sample.test.demo.tests.base.BaseTest;
import com.sample.test.demo.utils.testdata.Pizza;
import com.sample.test.demo.utils.testdata.TestData;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Epic("Happy Path test cases")
public class HappyPathTest extends BaseTest {

    @Test(dataProvider = "differentPizzas", dataProviderClass = TestData.class, description = "Order pizza different pizzas")
    @Description("Happy path for scenario when we order some pizza")
    @Story("Order Pizza")
    public void orderDifferentPizzas(Pizza pizza) {
        orderPizzaPage.choosePizza(pizza);
        orderPizzaPage.setQuantityOfPizza(pizzaQuantity);
        orderPizzaPage.enterDefaultContactInformation();
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with success information about his order")
                .startsWith("Thank you for your order!")
                .contains(pizza.getName());

        orderPizzaPage.closeDialogMessage();
    }

    @Test(dataProvider = "paymentType", dataProviderClass = TestData.class, description = "Order pizza with different payment options")
    @Description("Happy path for scenario when we order some pizza with different payment options")
    @Story("Order Pizza")
    public void orderPizzaWithDifferentPaymentOptionsTest(By paymentType) {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.SMALL_NOTOPPINGS.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza(pizzaQuantity);
        orderPizzaPage.enterDefaultContactInformation();
        orderPizzaPage.selectPaymentOption(paymentType);
        orderPizzaPage.orderPizza();

        assertThat(orderPizzaPage.getDialogMessage())
                .as("Verify that user see dialog message with success information about his order")
                .startsWith("Thank you for your order!")
                .contains(PizzaTypes.SMALL_NOTOPPINGS.getDisplayName());

        orderPizzaPage.closeDialogMessage();
    }

    @Test(description = "Verify reset behaviour")
    @Description("Verify happy path for the reset behaviour for the order page")
    @Story("Order Pizza")
    public void resetOrderBehaviourTest() {
        orderPizzaPage.choosePizza(Pizza.builder()
                .name(PizzaTypes.MEDIUM_TWOTOPPINGS.getDisplayName())
                .firstTopping(PizzaToppings.OLIVES.getDisplayName())
                .secondTopping(PizzaToppings.SALAMI.getDisplayName())
                .build());
        orderPizzaPage.setQuantityOfPizza(pizzaQuantity);
        orderPizzaPage.enterDefaultContactInformation();
        orderPizzaPage.selectPaymentOption(Locators.CREDIT_CARDS.getByLocators());
        orderPizzaPage.resetOrderPizza();

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(orderPizzaPage.getValueOfField(Locators.PIZZA_DROPDOWN.getByLocators()))
                .as("Verify that we don't have preselected Pizza")
                .isEqualTo("Choose Pizza");
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.FIRST_TOPPING_DROP_DOWN.getByLocators()))
                .as("Verify that we don't have preselected first topping")
                .isEqualTo("Choose a Toppings 1");
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.SECOND_TOPPING_DROP_DOWN.getByLocators()))
                .as("Verify that we don't have preselected second topping")
                .isEqualTo("Choose a Toppings 2");
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.PIZZA_QUANTITY.getByLocators()))
                .as("Verify that we don't have preselected quantity")
                .isEqualTo("0");
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.COST.getByLocators()))
                .as("Verify that we don't have preselected cost")
                .isEqualTo("0");
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.NAME_FIELD.getByLocators()))
                .as("Verify that we don't have preselected name")
                .isEmpty();
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.EMAIL_FILED.getByLocators()))
                .as("Verify that we don't have preselected email")
                .isEmpty();
        softly.assertThat(orderPizzaPage.getValueOfField(Locators.PHONE_FIELD.getByLocators()))
                .as("Verify that we don't have preselected phone")
                .isEmpty();

        softly.assertAll();
    }
}
