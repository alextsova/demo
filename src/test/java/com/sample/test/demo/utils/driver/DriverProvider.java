package com.sample.test.demo.utils.driver;

import com.sample.test.demo.utils.PropertyConfiguration;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;

public class DriverProvider {
    private PropertyConfiguration propertyConfiguration = PropertyConfiguration.getInstance();
    private IWebDriver driver;

    @Step("Create Selenium Web Driver")
    public IWebDriver getDriver() {

        switch (propertyConfiguration.getBrowser()) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver = new WebDriverChromeImplementation();
                break;
            case "safari":
                WebDriverManager.safaridriver().setup();
                driver = new WebDriverSafariImplementation();
                break;
            default:
                driver = new WebDriverChromeImplementation();
        }

        driver.configuration();

        return driver;
    }
}
