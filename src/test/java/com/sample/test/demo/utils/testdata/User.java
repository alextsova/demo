package com.sample.test.demo.utils.testdata;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class User {
    private String userName;
    private String email;
    private String phoneNumber;
}
