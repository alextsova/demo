package com.sample.test.demo.utils;

import io.qameta.allure.Step;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.testng.Assert.assertNotNull;

public class PropertyConfiguration {

    private static final String CONFIG_FILE_NAME = "config.properties";
    private Properties configProperties;
    private static PropertyConfiguration configuration;

    private PropertyConfiguration() {
        loadProperties();
    }

    public static PropertyConfiguration getInstance() {
        if (configuration == null) {
            configuration = new PropertyConfiguration();
        }

        return configuration;
    }

    @Step("Load properties from configuration file")
    private void loadProperties() {
        configProperties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assertNotNull(classLoader);
        InputStream inputStream = classLoader.getResourceAsStream(CONFIG_FILE_NAME);
        try {
            configProperties.load(inputStream);
        } catch (final IOException ignored) {
        }
    }

    public String getBrowser() {
        return getProperty("browser");
    }

    public String getPlatform() {
        return getProperty("platform");
    }

    public String getUrl() {
        return getProperty("url");
    }

    public String getProperty(String propertyName) {
        return configProperties.getProperty(propertyName);
    }
}
