package com.sample.test.demo.utils.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public interface IWebDriver {

    WebElement getElementBySelector(By bySelector);

    void clickElementBySelector(By bySelector);

    void enterTextToElementBySelector(By bySelector, String text);

    Select getSelectBy(By bySelector);

    String getValueAttributeBy(By bySelector);

    void configuration();

    void openPage(String pageUrl);

    void quit();

    WebDriver getWebDriver();
}
