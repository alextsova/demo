package com.sample.test.demo.utils.testdata;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class Pizza {
    private String name;
    private String firstTopping;
    private String secondTopping;
}
