package com.sample.test.demo.utils.testdata;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UserProvider {

    public User getDefaultUser() {
        return User.builder()
                .userName("Alex")
                .email("test@mail.com")
                .phoneNumber("123456789")
                .build();
    }
}
