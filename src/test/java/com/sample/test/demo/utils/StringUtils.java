package com.sample.test.demo.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Optional;

@UtilityClass
public class StringUtils {

    public String randomStringWith101Chars() {
        return RandomStringUtils.random(101, true, false);
    }

    public String defaultValueIfNull(String value) {
        return Optional.ofNullable(value).orElse("");
    }
}
