package com.sample.test.demo.utils.allure;

import com.sample.test.demo.tests.base.BaseTest;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {
    @Attachment(value = "{0}", type = "text/plain")
    private static String saveTextLog(String message) {
        return message;
    }

    @Attachment(value = "{0}", type = "text/html")
    public static String attachHtml(String html) {
        return html;
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] attachDeviceScreenshot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Object testClass = result.getInstance();
        WebDriver driver = ((BaseTest) testClass).getDriver().getWebDriver();

        //Allure ScreenShotRobot and SaveTestLog
        if (driver != null) {
            attachDeviceScreenshot(driver);
        }
        //Save a log on allure.
        saveTextLog(result.getThrowable().toString());
    }
}
