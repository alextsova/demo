package com.sample.test.demo.utils.driver;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WebDriverChromeImplementation implements IWebDriver {

    @Getter
    private WebDriver webDriver;
    private WebDriverWait wait;

    public WebDriverChromeImplementation() {
        this.webDriver = new ChromeDriver();
        this.wait = new WebDriverWait(webDriver, 10, 1000);
    }

    @Override
    public WebElement getElementBySelector(By bySelector) {
        return wait.until(d -> webDriver.findElement(bySelector));
    }

    @Override
    public void clickElementBySelector(By bySelector) {
        getElementBySelector(bySelector).click();
    }

    @Override
    public void enterTextToElementBySelector(By bySelector, String text) {
        getElementBySelector(bySelector).clear();
        getElementBySelector(bySelector).sendKeys(text);
    }

    @Override
    public Select getSelectBy(By bySelector) {
        return new Select(wait.until(d ->webDriver.findElement(bySelector)));
    }

    @Override
    public String getValueAttributeBy(By bySelector) {
        return getElementBySelector(bySelector).getAttribute("value");
    }

    @Override
    public void configuration() {
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public void openPage(String pageUrl) {
        webDriver.get(pageUrl);
    }

    @Override
    public void quit() {
        webDriver.quit();
    }
}
