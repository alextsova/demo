package com.sample.test.demo.utils.testdata;

import com.sample.test.demo.constants.Locators;
import com.sample.test.demo.constants.PizzaToppings;
import com.sample.test.demo.constants.PizzaTypes;
import org.testng.annotations.DataProvider;

public class TestData {

    @DataProvider
    public static Object[][] differentPizzas() {
        return new Object[][]{
                {Pizza.builder().name(PizzaTypes.SMALL_NOTOPPINGS.getDisplayName()).build()},
                {Pizza.builder().name(PizzaTypes.LARE_NOTOPPINGS.getDisplayName()).build()},
                {Pizza.builder().name(PizzaTypes.SMALL_ONETOPPINGS.getDisplayName()).firstTopping(PizzaToppings.OLIVES.getDisplayName()).build()},
                {Pizza.builder().name(PizzaTypes.MEDIUM_TWOTOPPINGS.getDisplayName()).firstTopping(PizzaToppings.OLIVES.getDisplayName()).secondTopping(PizzaToppings.SALAMI.getDisplayName()).build()},
                {Pizza.builder().name(PizzaTypes.LARGE_TWOTOPPINGS.getDisplayName()).firstTopping(PizzaToppings.MUSHROOMS.getDisplayName()).secondTopping(PizzaToppings.EXTRACHEESE.getDisplayName()).build()}
        };
    }

    @DataProvider
    public static Object[][] paymentType() {
        return new Object[][]{
                {Locators.CREDIT_CARDS.getByLocators()},
                {Locators.CASH.getByLocators()}};
    }
}
