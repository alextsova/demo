package com.sample.test.demo.pages;

import com.sample.test.demo.utils.driver.IWebDriver;
import io.qameta.allure.Step;

public class BasePage {
    protected IWebDriver webDriver;

    public BasePage(IWebDriver webDriver) {
        this.webDriver = webDriver;
    }

    @Step("Open page: {pageUrl}")
    public void openPage(String pageUrl) {
        webDriver.openPage(pageUrl);
    }
}
