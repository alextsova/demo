package com.sample.test.demo.pages;

import com.sample.test.demo.constants.Locators;
import com.sample.test.demo.utils.StringUtils;
import com.sample.test.demo.utils.driver.IWebDriver;
import com.sample.test.demo.utils.testdata.Pizza;
import com.sample.test.demo.utils.testdata.UserProvider;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class OrderPizzaPage extends BasePage {

    public OrderPizzaPage(IWebDriver webDriver) {
        super(webDriver);
    }

    @Step("Choose pizza: {pizza}")
    public void choosePizza(Pizza pizza) {
        selectPizza(StringUtils.defaultValueIfNull(pizza.getName()));
        selectFirstToppings(StringUtils.defaultValueIfNull(pizza.getFirstTopping()));
        selectSecondToppings(StringUtils.defaultValueIfNull(pizza.getSecondTopping()));
    }

    @Step("Select pizza by name: {pizzaName}")
    public void selectPizza(String pizzaName) {
        webDriver.getSelectBy(Locators.PIZZA_DROPDOWN.getByLocators()).selectByValue(pizzaName);
    }

    @Step("Enter quantity of pizza: {quantityOfPizza}")
    public void setQuantityOfPizza(String quantityOfPizza) {
        webDriver.enterTextToElementBySelector(Locators.PIZZA_QUANTITY.getByLocators(), quantityOfPizza);
    }

    @Step("Enter default contact information")
    public void enterDefaultContactInformation() {
        filledUpNameField(UserProvider.getDefaultUser().getUserName());
        filledUpEmailField(UserProvider.getDefaultUser().getPhoneNumber());
        filledUpPhoneField(UserProvider.getDefaultUser().getPhoneNumber());
    }

    @Step("Enter name in name field: {name}")
    public void filledUpNameField(String name) {
        webDriver.enterTextToElementBySelector(Locators.NAME_FIELD.getByLocators(), name);
    }

    @Step("Enter name in email field: {email}")
    public void filledUpEmailField(String email) {
        webDriver.enterTextToElementBySelector(Locators.EMAIL_FILED.getByLocators(), email);
    }

    @Step("Enter name in phone field: {phone}")
    public void filledUpPhoneField(String phone) {
        webDriver.enterTextToElementBySelector(Locators.PHONE_FIELD.getByLocators(), phone);
    }

    @Step("Select payment options: {paymentOption}")
    public void selectPaymentOption(By paymentOption) {
        webDriver.clickElementBySelector(paymentOption);
    }

    @Step("Tap on the Place Order button for creating order")
    public void orderPizza() {
        webDriver.clickElementBySelector(Locators.ORDER_PIZZA_BUTTON.getByLocators());
    }

    @Step("Tap on the Reset button")
    public void resetOrderPizza() {
        webDriver.clickElementBySelector(Locators.RESET_ORDER_BUTTON.getByLocators());
    }

    @Step("Get text from the dialog message")
    public String getDialogMessage() {
        return webDriver.getElementBySelector(Locators.DIALOG_POPUP.getByLocators()).getText();
    }

    @Step("Select first topping for pizza: {toppingName}")
    public void selectFirstToppings(String toppingName) {
        webDriver.getSelectBy(Locators.FIRST_TOPPING_DROP_DOWN.getByLocators()).selectByValue(toppingName);
    }

    @Step("Select second topping for pizza: {toppingName}")
    public void selectSecondToppings(String toppingName) {
        webDriver.getSelectBy(Locators.SECOND_TOPPING_DROP_DOWN.getByLocators()).selectByValue(toppingName);
    }

    @Step("Close dialog message")
    public void closeDialogMessage() {
        webDriver.clickElementBySelector(Locators.DIALOG_CLOSE_BUTTON.getByLocators());
    }

    @Step("Get value of the field: {bySelector}")
    public String getValueOfField(By bySelector) {
        return webDriver.getValueAttributeBy(bySelector);
    }
}
