package com.sample.test.demo.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openqa.selenium.By;

@Getter
@AllArgsConstructor
public enum Locators {
    PIZZA_DROPDOWN(By.id("pizza1Pizza")),
    FIRST_TOPPING_DROP_DOWN(By.className("toppings1")),
    SECOND_TOPPING_DROP_DOWN(By.className("toppings2")),
    PIZZA_QUANTITY(By.id("pizza1Qty")),
    COST(By.id("pizza1Cost")),
    NAME_FIELD(By.id("name")),
    EMAIL_FILED(By.id("email")),
    PHONE_FIELD(By.id("phone")),
    CASH(By.id("cashpayment")),
    CREDIT_CARDS(By.id("ccpayment")),
    ORDER_PIZZA_BUTTON(By.id("placeOrder")),
    RESET_ORDER_BUTTON(By.id("reset")),
    DIALOG_POPUP(By.id("dialog")),
    DIALOG_CLOSE_BUTTON(By.cssSelector("button[title='Close']"));

    private By byLocators;
}
